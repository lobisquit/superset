def compute_superset(elements):
    masks = [1 << digit for digit in range(len(elements))]

    last_subset_idx = 1 << len(elements) # 2 ** len(elements)
    for i in range(1, last_subset_idx):
        subset = []

        for digit in range(len(elements)):
            if (i & masks[digit]) > 0:
                subset.append(elements[digit])

        yield subset

if __name__ == '__main__':
    elements = input().split(",")
    for subset in compute_superset(elements):
        print(",".join(subset))
