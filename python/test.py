import unittest

from parameterized import parameterized_class

from main import compute_superset


@parameterized_class([{ "N": n, } for n in range(15)])
class SupersetTestCase(unittest.TestCase):
    def setUp(self):
        self.set = [f"{i}" for i in range(self.N)]
        self.superset = compute_superset(self.set)

    def test_size(self):
        superset_size = sum(1 for _ in self.superset)
        expected_size = 2 ** len(self.set) - 1

        self.assertEqual(superset_size, expected_size)

    def test_uniqueness(self):
        # subsets are converted in a unique string representation that
        # can be put into a set, to remove duplicates
        unique_subsets = {",".join(sorted(subset)) for subset in self.superset}
        expected_size = 2 ** len(self.set) - 1

        self.assertEqual(len(unique_subsets), expected_size)

    def test_subsets_of_original_set(self):
        for subset in self.superset:
            for element in subset:
                self.assertTrue(element in self.set)


if __name__ == '__main__':
    unittest.main()
