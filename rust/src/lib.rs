fn compute_nth_subset<'a>(elements: &'a Vec<&str>, n: u64) -> Vec<&'a str> {
    let mut subset = vec![];

    for digit in 0..elements.len() {
        let mask: u64 = 1 << digit;

        if n & mask != 0 {
            subset.push(elements[digit]);
        }
    }

    subset
}

pub fn compute_superset<'a>(set: &'a Vec<&'a str>) -> impl Iterator<Item = Vec<&'a str>> + 'a {
    let n_subsets = 1 << set.len();
    (1..n_subsets)
        .map(move |n| compute_nth_subset(&set, n))
        .into_iter()
}
