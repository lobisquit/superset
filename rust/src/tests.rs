use crate::lib::compute_superset;
use std::collections::BTreeSet;

static N: u64 = 16;

fn setup_set(n: u64) -> Vec<String> {
    (1..n).map(|x| format!("{}", x)).map(|x| x).collect()
}

#[test]
fn test_size() {
    for n in 0..N {
        let set = setup_set(n);
        let set = set.iter().map(|x| &x as &str).collect();
        let superset = compute_superset(&set);

        let expected_size = 2_usize.pow(set.len() as u32) - 1;
        let superset_size: usize = superset.map(|_| 1).sum();

        assert!(superset_size == expected_size);
    }
}

#[test]
fn test_uniqueness() {
    for n in 0..N {
        let set = setup_set(n);
        let set = set.iter().map(|x| &x as &str).collect();
        let superset = compute_superset(&set);

        let expected_size = 2_usize.pow(set.len() as u32) - 1;

        let unique_sets: BTreeSet<BTreeSet<&str>> =
            superset.map(|x| x.iter().map(|x| *x).collect()).collect();
        assert!(unique_sets.len() == expected_size);
    }
}

#[test]
fn test_original_elements() {
    for n in 0..N {
        let set_vec = setup_set(n);
        let set_vec = set_vec.iter().map(|x| &x as &str).collect();

        let superset: Vec<BTreeSet<&str>> = compute_superset(&set_vec)
            .map(|x| x.iter().map(|x| *x).collect())
            .collect();

        let set: BTreeSet<&str> = set_vec.iter().map(|x| &x as &str).collect();
        for subset in superset {
            assert!(subset.is_subset(&set));
        }
    }
}
