use crate::lib::compute_superset;
use std::io::{stdin, Read};

mod lib;

#[cfg(test)]
mod tests;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // read elements from stdin
    let mut buffer = String::new();
    stdin().read_to_string(&mut buffer)?;

    let elements: Vec<&str> = buffer.split(',').map(|x| x.trim_end()).collect();

    // print to output, one per line
    for subset in compute_superset(&elements) {
        println!("{}", subset.join(","));
    }

    Ok(())
}
